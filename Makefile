
# Load in new config settings
include _CONFIG.txt

# This combines all the filepaths in SECTIONS_FILEPATH file
SECTIONS := $(shell cat $(SECTIONS_FILEPATH) | tr '\n\r' ' ' | tr '\n' ' ' )

TEMPLATE_DIR=$(shell realpath $(shell dirname $(TEMPLATE)))

TEX_CMD=TEXINPUTS=$(TEXINPUTS):$(TEMPLATE_DIR) pandoc --toc --template=../$(TEMPLATE) --top-level-division=chapter

.PHONY: all clean html pdf
default: pdf
.DEFAULT_GOAL := pdf


pre:
	mkdir -p build

clean:
	rm -rf build

all: pdf tex html ebook mobi

# build the document
pdf: pre
		cd ./source/  && \
		$(TEX_CMD) -o ../build/$(BUILDNAME).pdf $(SECTIONS)

# only build tex (good for migrating to tex-only build system)
tex: pre
		cd ./source/  && \
		$(TEX_CMD) -o ../build/$(BUILDNAME).tex $(SECTIONS)

# build as document as html
html: pre
		cd ./source/ && \
		pandoc --reference-links --webtex --section-divs -s --biblatex --toc -N --bibliography=$(REFERENCES) -o ../build/$(BUILDNAME).html -t html --css ../$(HTML_STYLE) $(SECTIONS)

ebook: pre
		cd ./source/ && \
		pandoc --toc -N --webtex --standalone --self-contained -o ../build/$(BUILDNAME).fb2  $(SECTIONS)

mobi: ebook
		cd ./source/ && \
		ebook-convert ../build/$(BUILDNAME).fb2 ../build/$(BUILDNAME).mobi > /dev/null


# build a presentation (from a single file, and not all)
pres: pre
		cd ./source/ && \
		pandoc -s --self-contained --mathjax --reference-links -i -t revealjs -o ../build/$(BUILDNAME).pres.html  --slide-level=2 ../$(PRESENTATION)
