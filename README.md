
# Integriertes Netz und System management

Aufzeichnungen zur Vorlesung *Integriertes Netz und System Mangagement* SS2018 am KIT.

Im wesentlichen ist das eine Liste der wichtigsten Stichwort, die in der Vorlesungen gefallen sind mit einer kurzen Erklärung.



## quickstart

	sudo apt install texlive-full evince pandoc pandoc-citeproc
	git clone --recurse-submodules git@gitlab.com:wotanii/markdown-latex-boilerplate.git
	cd markdown-latex-boilerplate
	make pdf
	envince build/example.pdf

you will want to configure the project to your needs:

  * `template/template.tex` for your formatting
  * `example.md` for your content
  * `_CONFIG.txt` for general parameters
  * `_SECTIONS.txt` to order of your source-files.

When you want to add a section "conclusion", you add the file "conclusion.md" to "source" and then you add "conclusion.md" to `_SECTIONS.txt`


## Requirements

* pandoc > 2.0,
  * https://github.com/jgm/pandoc/releases
* latex: `sudo apt install texlive-full `
* csl in csl subfolder: `git submodule update --recursive --remote`
* if you want build mobi, you need calibre ("ebook-convert" is part of the calibre-package): `sudo apt install calibre`

recommended modules:

* pandoc-shortcaption
* pandoc-citeproc: `sudo apt install pandoc-citeproc`
* pandoc-crossref

## Configering the build system

in config text file `config.txt`, you may have these default entries

    SECTIONS_FILEPATH=_SECTIONS.txt
    BUILDNAME=example
    REFERENCES=references.bib
    TEMPLATE=template.tex
    # TEMPLATE=ut-thesis.tex
    CSL=../elsevier-with-titles

<!-- _fix syntaxhighlighting in atom_ -->

This is what it means

	SECTIONS_FILEPATH=
		Holds a link to a file
		that contains a list
		of pages to join

	BUILDNAME=
		Let's you define the name of the generated file in the /build/ folder.

	REFERENCES=
		Links to pandoc reference file
		for the biography

	TEMPLATE=
		Links to template file use by LaTeX

	CSL=
		Choose from a list of Citation Style Language
		files found in ./CSL/ e.g. IEEE style.

Note:
 * `#` are comments.
 * You cannot have spaces in your key=value like `TEMPLATE = ut-thesis.tex`


### SECTIONS_FILEPATH

The file in SECTIONS_FILEPATH might look like this:

```
    example.md references.md
```

Where each file is entered in a single line delimitated by a single ` ` in sequence.

The reason is because the build file is automagically copying the content from `sections.txt` directly into `$(SECTIONS)` in for example the pandoc build html command line: `pandoc -S -5 ...etc... -t html --normalize $(SECTIONS)`. (e.g. `pandoc -S -5 ...etc... -t html --normalize example.md references.md`


## running the build

	make

After this the compiled document can be found in folder `build`

For more build targets, see `Makefile`

## Tips and Tricks


### auto build and preview

	evince build/example.pdf &; while inotifywait -e close_write source; do make pdf; done

### windows

[the upstream repository](https://github.com/davecap/markdown-latex-boilerplate) was focusing on windows-user and contains a lot of useful tipps
