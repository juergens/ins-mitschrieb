FROM ubuntu

RUN apt-get update && apt-get dist-upgrade -y
RUN apt-get install -y calibre texlive-full evince pandoc pandoc-citeproc

# gitlab can't handle submodules
RUN apt-get install -y ssh git

RUN apt-get install -y build-essential

ENV PANDOC_URL https://github.com/jgm/pandoc/releases/download/2.2.2.1/pandoc-2.2.2.1-1-amd64.deb

# pandoc in ubuntu-repo is outdated.
RUN TEMP_DEB="$(mktemp)" && \
	wget -O "$TEMP_DEB" "$PANDOC_URL" && \
	dpkg -i "$TEMP_DEB" && \
	rm -f "$TEMP_DEB"



# this dockerfile is for the ci-runner
