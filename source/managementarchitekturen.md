# Managementarchitekturen

## Teilmodelle

### Informationsmodell

Pro Kompenente

Anforderungen:

- allgemein verwendbar (insb. nicht gerätspezifisch)
- standardisert
- relevant für Verwaltung der Komponente

Aktionen:

- Lesender/Schreiben Zugriff von Mangager
- Alarm zu Manager
- Option für weitere Operation

#### MO - Managed Object

- **Attribute** für Managementinformationen
- **Operation** auf Attribute
- **Kopplung** zur Kompenente

![](assets/markdown-img-paste-20180721120914784.png)

Hinweis: "MO" beschreibt die Klasse, nicht die konkrete Instanz

#### MIB - Management Information Base

i.d.R. haben Netzkomponenten mehrere MO.

MIB = Menge von MO

MIB wird verwaltet von:

- Managed Entity (also der Komponente selbst)
- oder Agent

![](assets/markdown-img-paste-2018072112140807.png)

### Kommunikationsmodell

- Monitory:
	- Manager ließt Information
	- initialisiert von Manager
- Controlling:
	- Mangager schreibt Information
	- initialisiert von Manager
- Ereignismeldung:
	- Agent sendet Information zu Manager
	- initialisiert von MO/Agent
	- MO --> MIB --> Agent --> Manager


![allgemeine Ablauf](assets/markdown-img-paste-20180721122019252.png)

### Organisationsmodell

übliche Kategorien:

- Manager of Managers
	- MoM verwaltet Manager
	- Manager verwaltet Subdomäne
- verteiltes Management
	- Mehrere Manager pro Domäne
	- Manager "kennen" sich untereinander
	- ggf. mit standy-by manager
- Intelligente Agenten / Self-management
	- Agenten übernehmen funktion von verteilten Managern
	- Vorteil:
		- Ausfall von Manager: Agenten funkionieren trotzdem
		- Bei Netzpartitionierung: Management trotzdem

Management-by-...

- Management-by-Delegation: Manager delegiert Aufgabe an Agent
- Management-by-Objectives / Policy-based Management: Manager legt Ziele/Richtlinien fest und Agent entscheidet selbst

## Funktionsmodell

- Modellierung der Funktionen des Managentes
- Festlegen generischer Funkionen
- Zuordnung von Diensten zu Funktionen

![](assets/markdown-img-paste-20180721123121125.png)

### FCAPS

- **F**ault
- **C**onfiguration
- **A**ccounting
- **P**erformance
- **S**ecurity

#### Konfigurationsmanagement

Konfiguration:

- Charakterisierugn der Ressource _(z.B. OS-Version)_
- Zuweisung von (eindeutigen) Namen und Kennungen _(z.B. IP-Addr.)_
- Änderung von Parametern _(z.B. ACL auf Borderrouter)_
- History / Abspeichern des Ergebnisses der Konfiguration

Konfigurationsmanagement:

- Erfassung von Systemkonfigurationen
- Dokumentation von Systemkonfigurationen
- Change Management

Bausteine:

- Autokonfiguration / Auto-Discovery
- Dokumentationssysteme / Configuration Mgmt. DB (CMDB)
- Visualisierungswerkzeuge _(z.B. für Netztopologie)_
- Werkzeuge zur Software-Verteilung und Lizenzüberwachung

#### Fehlermanagement

Aufgaben:

- Entdecken, Eingrenzen und Beheben von abnormalem Systemverhalten

#### Leistungsmanagement

Aufgaben:

- Aufrechterhaltung/Steigerung der Dienstgüte (QoS)

Parameter von Dienstgüte:

- Bandbreite
- Verzögerung
- (Delay-) Jitter
- Fehlerraten

#### Abrechnungsmanagement

Welche Person/Organisation hat wieviel Last verursacht?

Bausteine:

- Verbrauchsdaten
- Abrechnungsdaten
- Überwachen von Kontingenten

#### Sicherheitsmanagement

Aufgaben:

- Durchsetzung von Sicherheitsrichtlinien
- Autorisierung
- Zugriffskontrolle
- Festellen von Angriffen


## Delegierung von Funktionalität

Idee: Delegieren von Funktionen an Agenten

Vorteil: Skalierbarkeit, Load Sharing, Genauigkeit, Netzlast, ...

Nachteil: Management komplexer


- Wie? *statisch* vs *dynamisch*
- Wann? *Ereignisgesteuert*, *Benutzergesteuert*, *Bei Änderung*
- Wie lang? *einmalig, kontinuierlich, Ereignisgesteuert*

Herausforderungen:

- Funktionen müssen Delegierbar gemacht werden
- Managementprotokoll muss Delegierung unterstützen
- Manager müssen wissen was wohin deligiert ist
- Agenten müssen Funkion implementieren

![](assets/markdown-img-paste-20180721125958422.png)

## Zeitbereiche des Managements

![](assets/markdown-img-paste-20180721125809469.png)
