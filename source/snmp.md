# SNMP - Internet Management

## Vor-/Nachteile des SNMP-SMI-Ansatzes

Vorteile:

- einheitliche Namenstruktur (dank OID-Baum)
- einfach (leichtes Implementieren, wenig Fehleranfällig)

Nachteile:

- keine logische Struktur
- schwieriges Modellieren komplexer Zussamenhänge


## Informationsmodell

### MO - Managed Object

#### Identifikation

Problem: Weltweit eindeutiger Name

Lösung: OID - Object Identifier

- erster Teil: Organsiations-Struktur *(iso.org.did.internet)*
- mittlerer Teil: Logische Struktur *(mgmt.mib-2)*
- letzter Teil: Klasse
- ganz letzter Teil: ID der Instanz, Attribut (nicht Teil von OID, aber relevant in SNMP)

Beispiel: OID meines Systems

- in worten: `iso.org.dod.internet.management.mib-2.system.sysDescr`
- in Zahlen: `1.3.6.1.2.1.1.1`

![](assets/markdown-img-paste-20180722133608126.png)

![Baum bis mib-2](assets/markdown-img-paste-20180722133644987.png)

![aum ab mib-2](assets/markdown-img-paste-20180722133717336.png)

**Verwendung in SNMP**

- vorderer Teil: OID definiert Klasse
- hinterer Teil: Nummern definieren Datentypen und Instanzen

Beispiel:

- tcpConnLocalAddress (OID): 1.3.6.1.2.1.6.13.1.2
- tcpConnLocalAddress: 10.0.0.99
- tcpConnLocalPort: 12
- tcpConnRemAddress: 9.1.2.3
- tcpConnRemPort: 15
- --> Instanz:
- 1.3.6.1.2.1.6.13.1.2.**10.0.0.99.12.9.1.2.3.15**

Problem: Klassen sehen aus wie Instanzen

Lösung: Auf Klassen kann man nicht zugreifen / Es gibt keine Instanzen, die darauf antworten würden --> Problem löst sich von selbst


#### ASN.1 - Abstract Syntax Notation

Idee: Einheitliche Syntax, die kompakte binäre Repräsentation hat

Datentypen:

- primitiv
	- BOOLEAN
	- INTEGER
	- OCTET STRING
- strukturiert
	- SEQUENCE
	- SET
- other
	- CHOICE
	- ANY

![](assets/markdown-img-paste-20180722134321175.png)

Makro:

- Erweitern Syntax um neue Typen
- Man unterscheide:
	- Macro Definitionsformat
	- Macro Definition
	- Macro Instanz

![](assets/markdown-img-paste-20180722135322837.png)

Modul: Bündelung von
Datendefinitionen

![](assets/markdown-img-paste-20180722135339377.png)

**Transfer Syntax**

Transfer Syntax = Encoding Rules + Abstract Syntax

Encoding Rules:

- BER - Basic Encoding Rules
- TLV - Type, Length, Value

![Beispiel: Integer](assets/markdown-img-paste-20180722135714798.png)

#### SMI - Structure of Management Information


SMI definiert Regeln zur Definition von Objekten

- Nur objektbasierter, nicht objektorientiert
- basiert auf ASN.1
- Einfache typisierte Variablen
- Keine komplexe Datenstrukturen (insb. max 2-D Tabellen)

--> eingeschränkte Funktion von ASN.1

Datentypen zusätzlich zu ASN.1:

- SEQUENCE OF
	- für Abfrage von Tabellen
- Object Type Macro:
	- definiert, wie MO definiert werden
- vordefinierte Typen:
	- Counter „Zähler“
		- Zählt nach oben und mit Überlauf
		- Wenn der relative Wert wichtig ist
		- Bsp: Anzahl Packete
	- Gauge „Pegel“
		- Zählt in beide Richtunngen und ohne Überlauf
		- Wenn der absolute Wert wichtig ist
		- Bsp: Länge der Queue
	- IpAddress
	- ...

![Beispiel: ipRoutingTable in ASN.1](assets/markdown-img-paste-20180722154207699.png)


## Organisationsmodell

- n:m Beziehung:
	- 1 Manager kann viele Agenten haben
	- 1 Agent kann viele Manager haben
- Keine Manager-Manager-Kommunikation

Vokabeln:

- Management station:= "Manager"
- Managed station = "Agent"

![](assets/markdown-img-paste-20180722154457690.png)

## Kommunikationsmodell

![](assets/markdown-img-paste-20180722154531207.png)

- Polling: Manager fragt in regelmäßigen Abständen
die Agenten
- Traps: Agenten melden von sich aus an den Manager (i.d.R. in Ausnahmesituationen)
- Streng zentralisiertes Modell: Manager trägt gesamte Funktionalität und Verantwortung

UDP

- Ports 161, 162 (traps)
- Vorteil:
	- Bei Stau einsetzbar
	- Kein Kontext notwendig
	- keine Verbindungsabbrüche
- Nachteil:
	- unzuverlässig
	- Anwendung muss damit umgehen können

SNMP Community

- Manager und Agenten sind in einer *SNMP Community*
- Community Name:
	- Identifiziert Community
	- wird im Klartext übertragen
- Unsicheres Verfahren

## Funktionsmodell

## SNMPv1


![SNMPv1 PDU](assets/markdown-img-paste-20180722160238637.png)

*GetRequest*

- PDU-Type 0
- Bestandteile
	- RequestId: Unterscheidung zu anderen Requests
	- Var-Bindings: Namen (Was wird requested?)
	- Error-Status / Error-Index: immer 0
- Namen müssen komplett sein.

GetResponse

- PDU-Type 2
- Antwort auf Get, GetNext und Set
- Var-Bindings enthalten Werte

Set

- PDU-Type 3
- Wie Get, nur wird hier ein Wert mtgegeben

GetNext

- Problem: Get kann nur addressieren, was man kennt. Aber manchmal will man etwas neues Abfragen (z.B. 17. Zeile in Tabelle)
- Lösung: Man fragt den Nachfolger an
- PDU-Type 1
- GetNext-Request hat ID als Parameter (die nicht existieren muss)
- Response Antworted mit Instanz, die als erstes nach der ID kommt

Trap

- PDU-Type 4
- Angaben zum Sender und zum Event
- Default Traps z.B. für: reboot, failed auth, komp. offline, komp. online

![](assets/markdown-img-paste-20180722155849151.png)

Anm.: in SNMPv1 heißt jede Response "GetResponse"

## SNMPv2

- neue PDU:
	- GetBulk (schnelleres GetNext)
	- Inform
		- Trap, die Antwort bekommt
		- auch für Manager2Manager
- Sicherheit (wurde vorgeschlagen, jedoch nie umgesetzt)
- Funktionsmodell-Erweiterungen
	- Einfügen/Löschen von Tabellenzeilen durch den Manager
	- todo...

## SNMPv3

Problem: niemand benutzt snmpv2 für Konfiguration, da unsicher

Lösung: Sicherheit implementieren

todo...
