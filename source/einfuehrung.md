# Einführung


## noch zu lernen

### Allgemein

- FCAPS (Wörter)
- CIM?

### OSI

- TMN (Telecommunications Management Network)
- Informationsmodell
	- Strikte Mehrfachvererbung
	- Bäume:
		- Registration Tree
		- inheritance Tree
		- Containment Tree
- CMIS/CMIP
	- scoping
	- filtering
	- synchronization?

## Grundlagen

### physikalische Netzstruktur

- Geländeverkabelung
- Gebäudeverkabelung
- Etagenverkabelung

### VLAN

Problem: Broadcast schlecht bei großem Netz

Lösung: VLAN

Umsetzung:

- Broadcasts werden nur weitergeleiten an Ports, die zum gleichen VLAN gehören wie Sender
- Ethernetpackete bekommen tag (für normale Packete)
- Ports nach innen: feste Zurordnung von Port zu VLAN
- Ports nach außen: Akzeptiert nur ungetaggte Packete

### ACL - Access control lists

### Traffic Engineering

## Leistungsmanagement am Beispiel

Leistungsgrößen:

- Verfügbarkeit
	- zeitlich
	- örtlich
- Verzögerung / Latenz
- Durchsatz
	- min
	- avg
	- max/peak
- Kosten
	- TOC - Total Cost of Ownershup

### Verfügbarkeit - zeitlich

- MTBF - Mean Time between Failures
- MTTR - Mean Time to Repair

$$Availability = \frac{MTBF}{MTBF + MTTR}$$

### Netzdienste

- Vom Netz erbrachte Dienste
- Hilfdienste auf Anwendungsschicht
	- Mail *(SMTP)*
	- Verzeichnis *(DNS, LDAP)*
	- Zertifikatsdienste*(PKI)*

## Heterogenge Kompoenenten

- Endgeräte: *Großrechner, Handy, ...*
- Medien: *Glasfaser, Funk, ...*
- Netzkomponenten: *Hubs, Switches, Router, ...*
- Hersteller: *Cisco, HP, ...*
- Protokolle, Software, Schnittstellen
- Benachbarte Infrastruktur: *Verkehrswege, Stromleitungen, ...*

## Integriertes Management

Globale Anforderungen:

- Hohe Qualität des Kommunikationsdienstes
- Unterschiedliche Schnittstellen
- Netzkomponenten unterschiedlicher Qualität

Gesucht: Managementlösung für eigenes Netzwerk

(zwischen-) Ziele:

- Transparenz bzgl. Heterogenität
- Transparenz bzgl. Verteiltheit
- Funktionsbereiche vereinheitlichen
	- FCAPS: *Fehler-, Konfigurations-, Abrechnungs-, Leistungs-, Sicherheitsmanagement*
	- unterschiedliche Phasen: *kurzfristige, mittelfristige oder langfristige*

Daher benötigt integriertes Management:

- Herstellerneutrale Beschreibung von Managementinformationen
- Festlegung von Interfaces, Gateways, Datenformaten, Namenskonventionen für Managementzwecke
- Baukastensystem für Managementlösungen (Funktionsmodule, APIs)
- Gleiche Grundkonzepte für Netz-, System- u.Anwendungs-Management

Ansätze

- isoliert
- koordiniert
- integriert

![](assets/markdown-img-paste-20180721101851658.png)

--> für große Netze kommt nur der integrierte ansatz in Frage

vorteile von integriert:

- einfache Korrelation von Ereignissen: *Fehlersuche, Intrusion Detection, ...*
- einheitliches Arbeiten
- schnelle Integration von neuen Kompenenten (dank offener Schnittstellen)

nachteil:

- 100% integration nicht möglich
