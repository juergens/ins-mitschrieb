# OSI-Management

## allgemein

- Informationsmodell
	- objektorientiert (strict, multiple inheritance)
	- ASN.1-Template-Sprache (GDMO)
- Kommunikationsmodell
	- Systems Management, Layer Management, Layer Operation
	- CMIS/CMIP (scoping, filtering)
- Funktionsmodell
	- Systems Management Functional Areas SMFAs
	- Systems Management Functions SMFs
- Organisationsmodell
	- Manager/Agent
	- Domains, Policies

## OSI-Informationsmodell

- genannt: SMI - _structure of management Information_
- konsequent objekt-orientierter
- MO ist Instanze von MOC
- MOC - Managed object classes:
	- Attribute
		- Gruppenattribute
		- Mengenwertattribute
		- Sequenzen
	- Action
		- komplexer Operation, die mehrere Attribute ändert
		- vordefiniert: create, delete
	- Notification
	- Behaviour
		- Beschreibung von Attribute, Actions, Notifcations
		- i.d.R. in natürliche Sprache

- ISO Registrierungsbaum:
	- Wurzel "ROOT"
	- Vorderinierte MOC, Template u.ä.
- Vererbungsbaum
	- Wurzel "TOP"
- Enthaltenseinsbaum "Containment Tree"
	- Wurzel "SYSTEM"
	- MIT - Management Information Tree
	- Beschreibt die eigentliche Struktur des Systems

- Beziehungen zwischen MO
	- Enthaltensein
	- is-owned-by, is-backed-up-by
	- weitere definierbar über GRM - General Relationshop Model
		- vorderinierte operationen zum Verwalten der Beziehung (bind, unbind, notify …)
		- vorderinierte Rollen
		- vorderinierte Kardinalitäten

## Organisationsmodell

- Rollen:
	- Manager, Agent
	- alle systeme können beide Rollen (auch gleichzeitig)
	- zuordnung ändert sich dynamisch
- Organisationsdomänen
	- "functional domains"
	- Gruppierung nach Funktion und innerhalb von funktion nach Policies
- Verwaltungsdomänen
	- "administrative domains"
	- beschreibt welche MO
		- Verwaltungsauthorität haben
		- andere MO steuern können
		- welche Policies befolgen

## Kommunikationsmodell

- Ziel
	- steuern von ressourcen
- SMAP
	- "System Management Application Process"
	- Anwendung, die am Management teilnimmt
- SMAE
	- "Systeme Management Application Entity"
	- Der Teil des SMAP, der Kommunikation macht
- CMIS
	- "Common Management Information Service"
- CMIP
	- Protokoll, dass Kommunikation macht
	- verbindungsorientiert
	- gibt entfernten Zugriff auf
		- MO
		- Enthaltenseinsbaum
		- MIB
	- scoping
		- Man beschränkt sich auf einen Teilbaum im Containment Tree
		- optionen:
			- nur Basisobjekt
			- nur n-te Ebene unter Basisobjekt
			- Basisobjekt und alle Element bis n-te Ebene unter Basisobjekt
			- ganzer Teilbaum ab Basisobjekt
		- bsp. "alle Ports einer Karte"
	- filtering
		- über Existenz oder Werte von MO-Attribute
		- bsp. "von den Ports nur die mit Datenraten ab 1Gbit/s"
	- synchronization
		- atomic oder best-effort
		- atomic
			- vor ausführung der Operation wird ausführbarkeit bei allen Zielobjekten überprüft
			- --> ausführung findet überall statt, oder nirgends

## TMN - Telecommunications Management Network

- vereinheitlichendes Overlay für öffentlichen Netzwerk
- abstrahiert MO
- detailiert genug um damit Funktionen zu erfüllen (FCAPS)
- Bestandteile
	- TN "Telecommunication Network"
		- einzelne Teilnetze
		- z.B. ISDN, Mobilnetz
	- NE Network Element
		- einzelne Kompenente
		- z.B. Vermittlungsknoten
	- OS Operations System und OSF Operations System Function
		- einzelne Kompenente, die Managementinformationen verarbeitet
	- Mediation Device
		- weiterleiten von Managementinformationen
	- Workstation
		- Zugangspunkt von menschlichen Benutzer zu System
	- Data Communication Network
		- Komponente, die Kommunikation zwischen anderen Komponenten ermöglicht

## Zusammenfassung

- Informationsmodell
	- mächtig, flexibel einsetzbar
	- reichhaltige Strukturierungsmöglichkeiten
	- Akzeptanzprobleme wegen Komplexität
	- Offene Fragen:
		- Formale Spezifikation des MO-Verhaltens (Behaviour) -> Conformance?
		- Modellierung netzglobaler Ressourcen, Policies, Relations etc.
- Kommunikationsmodell:
	- nur schichtenübergreifendes Management behandelt (i.w.)
	- schichtenspezifisches Management nur in Ansätzen (LAN/MAN-Man.)
- Funktionsmodell:
	- umfangreiche Funktionalität in Form der SMFs vorhanden
	- z.T. extrem aufwendig zu implementieren, Profilbildung nötig
- Organisationsmodell:
	- Manager-Agent und Domänen-Modell bildet Referenz für aktuelle
Managementarchitekturen
