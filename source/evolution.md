
# Evolution des Netzwerkmanagements

## Informationsmodell

- SMIng - Structure of Management Information Next Generation
	- weiterentwicklung SMIv2 (von SNMP)

- CIM  - Common Information Model
	- XML
	- Kommunikation via http

## Kommunikationsmodell

- NetConf
	- XML basierte Konfiguration
- Management _von_ und _mit_ Web Services

## Organisations- und Funktionsmodell

- Trend zu Selbstorganisation und kooperativen Systeme
- PBNM Policy-based Network Management
	- Administrator gibt nur noch die Policy vor und muss sich nicht mit den verschiedenen Details rumärgern
